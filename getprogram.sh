#!/bin/bash

# Wymaga pakietu atool
# Wymaga wpisu w .ssh/config definiującego hosta mini

set -e

LOGIN=$2
STAGE=$1

DIR=/home2/samba/sobotkap/unix



for extension in bz2 gz; do
    for st in etap stage; do
	ST=$st
	EX=$extension
	FILENAME=${LOGIN}.${ST}${STAGE}
	if scp mini:$DIR/${FILENAME}.tar.${EX} .; then
	    break 2
	fi
    done
done

if [ -e ${FILENAME}/ ]; then
   echo Removing old files!!
   rm -rf ${FILENAME}/
fi
aunpack -D ${FILENAME}.tar.${EX}
cd ${FILENAME}

find -name '*.c' -exec gcc -pthread -Wall -Wextra {} \;

