#!/bin/bash

set -e

SERVER=mini
DIR=$1
DAY_AFTER_DEADLINE=$2
STUDENTS=$3

nameexpr=""

list=$(mktemp)


for s in $(cat ${STUDENTS}); do
    ssh "${SERVER}" find ../sobotkap/unix -maxdepth 1  -name "${s}*" \! -newermt "${DAY_AFTER_DEADLINE}"T00:00 >> ${list}
done

scp ${list} ${SERVER}:z/sop-to-download.txt
ssh "${SERVER}" tar -cf z/sop-to-download.tar -T z/sop-to-download.txt

scp "${SERVER}":z/sop-to-download.tar .
tar -xf sop-to-download.tar

ssh "${SERVER}" rm z/sop-to-download.tar z/sop-to-download.txt
rm sop-to-download.tar
rm ${list}
